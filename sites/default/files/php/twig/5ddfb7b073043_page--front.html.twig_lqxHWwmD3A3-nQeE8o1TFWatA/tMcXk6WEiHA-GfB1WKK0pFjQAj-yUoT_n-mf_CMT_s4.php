<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/telecommunication/page--front.html.twig */
class __TwigTemplate_0c0dcac0e93214fc1f4ef01553a790df5fc78b64a7c4d54e734713838ea71e9e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 17];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "  <header>
        <div class=\"Telecommunication-Top-header\">
            <div class=\"row\">
                <div class=\"col-md-5 col-sm-12 col-xs-12\">
                   <div class=\"Telecommunication-content\">
                    <div class=\"logo-content\">
                        <div class=\"logo-image\">
                            <img src=\"themes/telecommunication/assets/images/logo.png\" class=\"img-responsive\">
                        </div>
                        <div class=\"logo-text\">
                            <h2>GULF ARROWS FOR<br><span>TELECOMMUNICATION</span></h2>
                        </div>
                    </div>
                    <div id=\"boxshadow\">
                    </div>
                    <div class=\"container\">
                      ";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "telcommunication_info", [])), "html", null, true);
        echo "
                        <div class=\"read-more-btn\">
                            <a href=\"#\">Read More</a>
                        </div>
                    </div>
                </div>
</div>
                <div class=\"col-md-7 cols-m-6 col-xs-12\">
  <div class=\"background\">
      <div class=\"overlay\">

                       ";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slick_slider", [])), "html", null, true);
        echo "
                       
                                                          <div class=\"search\">
                                <div class=\"search-section\">
                                <div class=\"search-circle\">
                                    <a href=\"#\" class=\"search-btn\">Search</a>
                                    
                                </div>
                                 <input type=\"search\" class=\"form-control\" id=\"search\" placeholder=\"Search\" >
                               
                                <div class=\"dropdown\">
                                    <button class=\"btn\">Catagories<i class=\"fas fa-sort-down\"></i></button>
                                   
                                </div>
                            </div>
                              </div>
                               
                            
                            </div>
                            

                            <div class=\"main-nav\">
                                <nav class=\"navbar navbar-expand-lg navbar-light\">
                                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                                        <span class=\"navbar-toggler-icon\"></span>
                                    </button>

                                    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                                        <ul class=\"navbar-nav\">

                                        ";
        // line 58
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "telecommunication_navbar", [])), "html", null, true);
        echo "


                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
</div>
                    </div>
                </div>
    </header>
    <section>
        <div class=\"about-us-section\">
            <div class=\"row\">
                <div class=\"col-md-12 col-sm-12 col-xs-12\">
                  
";
        // line 75
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "about_us_section", [])), "html", null, true);
        echo "
                </div>
            </div>
            <div class=\"our-service\">
                <div class=\"container\">
                    <div class=\"our-service-content\">
                      <div class=\"row\">
                       <div class=\"col-md-5 col-sm-12 col-xs-12\">
                 ";
        // line 83
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "our_service", [])), "html", null, true);
        echo "
                        </div>
                        <div class=\"col-md-7 col-sm-12 col-xs-12\">
                       ";
        // line 86
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "our_service_box", [])), "html", null, true);
        echo "
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section>
    <div class=\"our-projects\">
       
        <div class=\"our-project-title\">
            <h2>OUR PROJECTS</h2>
        </div>
        <div class=\"row\">
        <div class=\"col-md-6 col-sm-12 col-xs-12\">
           ";
        // line 102
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "our_project", [])), "html", null, true);
        echo "
            </div>
            <div class=\"col-md-6 col-sm-6 col-xs-12\">
                 <div class=\"aside-images\">
                        
              <div class=\"row\">
                 ";
        // line 108
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "aside_images", [])), "html", null, true);
        echo "
                </div>
                    

                </div>
            </div>
        </div>
    </div>
</section>
<footer>

    <div class=\"footer\">
<div class=\"row\">
       <div class=\"col-md-3 col-sm-3 col-xs-12\">
        <div class=\"social-media\">
        ";
        // line 123
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "social_media", [])), "html", null, true);
        echo "
    </div>
        </div>
        <div class=\"col-md-4 col-sm-12 col-xs-12\">
           <div class=\"row\">
            <div class=\"footer_links\">
            ";
        // line 129
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_links", [])), "html", null, true);
        echo "
            
                </div>
                     <div class=\"footer_links2\">
                ";
        // line 133
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_links2", [])), "html", null, true);
        echo "
                </div>
            </div>
              
             
        </div>
        <div class=\"col-md-2 col-sm-12 col-xs-12\">
       ";
        // line 140
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_circle", [])), "html", null, true);
        echo "
        </div>
        <div class=\"col-md-3 col-sm-12 col-xs-12\">
            <div class=\"copyrights\">
";
        // line 144
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "copyrights", [])), "html", null, true);
        echo "
                        </div>
        </div>
        </div>
        </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "themes/telecommunication/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 144,  229 => 140,  219 => 133,  212 => 129,  203 => 123,  185 => 108,  176 => 102,  157 => 86,  151 => 83,  140 => 75,  120 => 58,  87 => 28,  73 => 17,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/telecommunication/page--front.html.twig", "C:\\wamp64\\www\\telecommunication\\themes\\telecommunication\\page--front.html.twig");
    }
}
