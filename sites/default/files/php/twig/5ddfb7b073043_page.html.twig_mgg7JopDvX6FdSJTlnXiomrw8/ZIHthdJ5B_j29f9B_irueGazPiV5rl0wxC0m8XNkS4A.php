<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/telecommunication/page.html.twig */
class __TwigTemplate_8dab4fcbb7664a4b1bddf04ae597a26ede8ff9f7c257f1b7d613fc78cfe1522b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 1, "if" => 2];
        $filters = ["escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $this->loadTemplate((($context["directory"] ?? null) . "/header.html.twig"), "themes/telecommunication/page.html.twig", 1)->display($context);
        // line 2
        echo "  ";
        if ($this->getAttribute(($context["page"] ?? null), "aside_services", [])) {
            // line 3
            echo "<div class=\"row\">
   <div class=\"col-md-8 col-sm-12 col-xs-12\">
    ";
            // line 5
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
</div>
<div class=\"col-md-4 col-sm-4  col-xs-12\">
    ";
            // line 8
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "aside_services", [])), "html", null, true);
            echo "

</div>
    </div>
";
        } elseif ($this->getAttribute(        // line 12
($context["page"] ?? null), "asie_projects", [])) {
            // line 13
            echo "<div class=\"row\">
   <div class=\"col-md-8 col-sm-12 col-xs-12\">
    ";
            // line 15
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
</div>
<div class=\"col-md-4 col-sm-4  col-xs-12\">
    ";
            // line 18
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "asie_projects", [])), "html", null, true);
            echo "

</div>
    </div>
";
        } elseif ($this->getAttribute(        // line 22
($context["page"] ?? null), "map", [])) {
            // line 23
            echo "    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13533.78417194304!2d35.85986653327045!3d32.0030292493999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151ca102cbe2787f%3A0xbdd6f87e40fa5da2!2sFikra%20for%20business%20development!5e0!3m2!1sen!2sjo!4v1574752907291!5m2!1sen!2sjo\"  height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>
 ";
        } elseif ($this->getAttribute(        // line 25
($context["page"] ?? null), "photo_gellery", [])) {
            // line 26
            echo "      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "photo_gellery", [])), "html", null, true);
            echo "
  ";
        } else {
            // line 28
            echo "    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
";
        }
        // line 30
        $this->loadTemplate((($context["directory"] ?? null) . "/footer.html.twig"), "themes/telecommunication/page.html.twig", 30)->display($context);
        // line 31
        echo "

";
    }

    public function getTemplateName()
    {
        return "themes/telecommunication/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 31,  118 => 30,  112 => 28,  106 => 26,  104 => 25,  98 => 23,  96 => 22,  89 => 18,  83 => 15,  79 => 13,  77 => 12,  70 => 8,  64 => 5,  60 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/telecommunication/page.html.twig", "C:\\wamp64\\www\\telecommunication\\themes\\telecommunication\\page.html.twig");
    }
}
