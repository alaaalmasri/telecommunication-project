<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/telecommunication/header.html.twig */
class __TwigTemplate_6451e5536404d550f70a1ebb9e200baa1d4b0c97ee0963078b8b012a593017b1 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 7];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "    <header>
       <div class=\"top-header2\">
           <div class=\"row\">
               <div class=\"col-md-6 col-sm-12 col-xs-12\">
                  <div class=\"logo-content2\">
                   <div class=\"logo-image2\">
                       <img src=\"";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "themes/telecommunication/assets/images/logo.png\" class=\"img-responsive\">
                                              <h2>GULF ARROW FOR <b>TELECOMMUNICATION</b></h2>

                   </div>
                  
                   </div>
               </div>
               <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            <div class=\"search2\">
                                <div class=\"search-section\">
                                <div class=\"search-circle2\">
                                    <a href=\"#\" class=\"search-btn2\">Search</a>
                                    
                                </div>
                                 <input type=\"search\" class=\"form-control\" id=\"search2\">
                               
                              
                            </div>
                              </div>
                              <div class=\"main-nav2\">
                                <nav class=\"navbar navbar-expand-lg navbar-light\">
                                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                                        <span class=\"navbar-toggler-icon\"></span>
                                    </button>

                                    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                                        <ul class=\"navbar-nav\">

                                ";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "telecommunication_navbar", [])), "html", null, true);
        echo "



                                        </ul>
                                    </div>
                                </nav>
                            </div>
               </div>
           </div>
       </div>
    </header>";
    }

    public function getTemplateName()
    {
        return "themes/telecommunication/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 35,  63 => 7,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/telecommunication/header.html.twig", "C:\\wamp64\\www\\telecommunication\\themes\\telecommunication\\header.html.twig");
    }
}
